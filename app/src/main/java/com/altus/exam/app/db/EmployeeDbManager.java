package com.altus.exam.app.db;

import android.content.Context;
import android.util.Log;

import com.altus.exam.app.model.Employee;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDbManager extends DatabaseManager {

    private static EmployeeDbManager instance;


    public static void init(Context ctx){
        if(null == instance){
            instance =  new EmployeeDbManager(ctx);
        }
    }

    public static EmployeeDbManager getInstance() {
        return instance;
    }

    private EmployeeDbManager(Context ctx) {
        super(ctx);
    }

    private GenericRawResults<String[]> genericEmployeeRawResults(String query) {
        GenericRawResults<String[]> rawResults = null;
        try {
            rawResults = getHelper().getEmployeeDao().queryRaw(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rawResults;
    }

    public int saveEmployee(Employee emp){
        try {
            return getHelper().getEmployeeDao().create(emp);
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int updateEmployee(int id, Employee emp){
        Log.d("Ferdz", id +" >> " + emp.toString());
        try {
            UpdateBuilder<Employee, Integer> updateBuilder = getHelper().getEmployeeDao().updateBuilder();
            updateBuilder.updateColumnValue(Employee.FIELD_LASTNAME, emp.getLastname());
            updateBuilder.updateColumnValue(Employee.FIELD_FIRSTNAME, emp.getFirstname());
            updateBuilder.updateColumnValue(Employee.FIELD_EMAIL, emp.getEmail());
            updateBuilder.where().idEq(id);
            return updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d("Ferdz", e.getMessage());
            return 0;
        }
    }

    public List<Employee> getAllEmployee(){
        try {
            return getHelper().getEmployeeDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return  null;
        }
    }

    public Employee getEmployeeData(String id){
        String query = rawQuery(Employee.TABLE_NAME_EMPLOYEE, new String[]{
                Employee.FIELD_EMPLOYEE_ID,
                Employee.FIELD_LASTNAME,
                Employee.FIELD_FIRSTNAME,
                Employee.FIELD_EMAIL} , Employee.FIELD_EMPLOYEE_ID +"=?", new String[]{id});
        GenericRawResults<String[]> rawResults =  genericEmployeeRawResults(query);

        List<Employee> employees = new ArrayList<Employee>();

        for(String[] results : rawResults){
            Employee empData = new Employee();
            empData.setId(Integer.parseInt(results[0]));
            empData.setLastname(results[1]);
            empData.setFirstname(results[2]);
            empData.setEmail(results[3]);
            Log.d("Altus",  empData.toString());
            employees.add(empData);
        }

        if(employees != null)
            return employees.get(0);

        return null;
    }

    public int deleteEmployee(Employee emp) {
        try {
            return getHelper().getEmployeeDao().delete(emp);
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
