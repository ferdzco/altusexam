package com.altus.exam.app.presenter;

import com.altus.exam.app.model.Employee;

public interface EmployeeContract {
    interface View {
        void showToastMessage(String message);
    }
    interface Presenter {
        void setEmployeeListener(EmployeePresenter.OnEmployeeListener listener);
        void loadExistingEmployee(int id);
        int saveEmployee(String firtsname, String lastname, String email);
        int updateEmployee(int id, String firtsname, String lastname, String email);
        int deleteEmployee(int id);
    }
}
