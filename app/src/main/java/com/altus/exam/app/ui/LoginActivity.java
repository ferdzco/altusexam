package com.altus.exam.app.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.altus.exam.app.presenter.LoginContract;
import com.altus.exam.app.presenter.LoginPresenter;
import com.altus.exam.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseAppCompatActivity implements LoginContract.View, LoginPresenter.OnLoginListener{

    @BindView(R.id.editEmail) EditText editEmail;
    @BindView(R.id.editPassword) EditText editPassword;

    private ProgressDialog progressDialog;
    private LoginContract.Presenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
        loginPresenter = new LoginPresenter(this, this);
        loginPresenter.setLoginListener(this);
    }

    @OnClick(R.id.btnLogin)
    public void login(View view){
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();
        loginPresenter.signIn(email, password);
    }

    @OnClick(R.id.textRegister)
    public void register(){
        startActivity(RegisterActivity.class);
    }

    @Override
    public void showProgressDialog() {
        progressDialog.setMessage("Logging in...");
        progressDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showToastMessage(String text) {
        toastMessage(text);
    }

    @Override
    public void handleLoginSuccess() {
        startActivity(MainActivity.class);
    }

    @Override
    public void habdleLoginFailed(String message) {
        toastMessage(message);
    }
}
