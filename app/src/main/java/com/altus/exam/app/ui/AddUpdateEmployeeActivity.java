package com.altus.exam.app.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.altus.exam.app.R;
import com.altus.exam.app.db.EmployeeDbManager;
import com.altus.exam.app.model.Employee;
import com.altus.exam.app.presenter.EmployeeContract;
import com.altus.exam.app.presenter.EmployeePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddUpdateEmployeeActivity extends BaseAppCompatActivity implements EmployeeContract.View,
        EmployeePresenter.OnEmployeeListener{

    public static final String ADD_Employee = "Add Employee";
    public static final String UPDATE_Employee = "Update Employee";

    private EmployeeContract.Presenter employeePresenter;
    private int mEmployeeId;

    @BindView(R.id.etLastname) EditText etLastname;
    @BindView(R.id.etFirstname) EditText etFirstname;
    @BindView(R.id.etEmail) EditText etEmail;
    @BindView(R.id.saveEmployee) Button saveEmployee;
    @BindView(R.id.deleteEmployee) Button deleteEmployee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_update_employee);
        ButterKnife.bind(this);

        String title = "Add Employee";

        employeePresenter = new EmployeePresenter(this, this);
        employeePresenter.setEmployeeListener(this);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            title = bundle.getString("action");
            saveEmployee.setText(ADD_Employee);
            if(title.equals("Update")){
                mEmployeeId = bundle.getInt("employeeId");
                employeePresenter.loadExistingEmployee(mEmployeeId);
                saveEmployee.setText(UPDATE_Employee);
                deleteEmployee.setVisibility(View.VISIBLE);
            }
        }

        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @OnClick(R.id.deleteEmployee)
    public void deleteEmployee(View view){
        int affectedRow = employeePresenter.deleteEmployee(mEmployeeId);
        if(affectedRow > 0){
            showToastMessage(" deleted successfully!");
        }
        finish();
    }

    @OnClick(R.id.saveEmployee)
    public void saveEmployee(View view){
        int affectRow = 0;
        String msg = "";

        String firstname = etFirstname.getText().toString();
        String lastname = etLastname.getText().toString();
        String email = etEmail.getText().toString();

        if(saveEmployee.getText().equals(UPDATE_Employee)){
            affectRow = employeePresenter.updateEmployee(mEmployeeId, firstname, lastname, email);
            msg = "Updated Succesfully!";
        }else{
            affectRow = employeePresenter.saveEmployee(firstname,
                    lastname,
                    email);
            msg = "Added Succesfully!";
        }

        if(affectRow > 0){
            showToastMessage(msg);
            etLastname.setText("");
            etFirstname.setText("");
            etEmail.setText("");
        }else{
            showToastMessage("Problem saving the data!");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void showToastMessage(String message) {
        toastMessage(message);
    }

    @Override
    public void handleExistingEmployee(Employee emp) {
        etLastname.setText(emp.getLastname());
        etFirstname.setText(emp.getFirstname());
        etEmail.setText(emp.getEmail());
    }
}
