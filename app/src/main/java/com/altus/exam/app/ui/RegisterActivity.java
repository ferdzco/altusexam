package com.altus.exam.app.ui;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.altus.exam.app.MyFireabase;
import com.altus.exam.app.R;
import com.altus.exam.app.presenter.LoginContract;
import com.altus.exam.app.presenter.RegisterContract;
import com.altus.exam.app.presenter.RegisterPresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseAppCompatActivity implements RegisterContract.View, RegisterPresenter.OnRegisterListener {

    private ProgressDialog progressDialog;
    private RegisterContract.Presenter registerPresenter;

    @BindView(R.id.editEmail) EditText editEmail;
    @BindView(R.id.editPassword) EditText editPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Register");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        registerPresenter = new RegisterPresenter(this, this);
        registerPresenter.setRegisterListener(this);

    }

    @OnClick(R.id.btnRegister)
    public void registerEmailAndPassword(View view){
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();
        registerPresenter.register(email, password);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home :
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProgressDialog() {
        progressDialog.setMessage("Register...");
        progressDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
    progressDialog.dismiss();
    }

    @Override
    public void showToastMessage(String text) {
        toastMessage(text);
    }

    @Override
    public void handleRegisternSuccess() {
        finish();
    }

    @Override
    public void habdleRegisterFailed(String message) {
        showToastMessage(message);
    }
}
