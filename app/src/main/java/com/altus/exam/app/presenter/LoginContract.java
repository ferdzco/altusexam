package com.altus.exam.app.presenter;


public interface LoginContract {

    interface View{
        void showProgressDialog();
        void dismissProgressDialog();
        void showToastMessage(String text);
    }

    interface Presenter{
        void setLoginListener(LoginPresenter.OnLoginListener listener);
        void signIn(String email, String password);
    }

}
