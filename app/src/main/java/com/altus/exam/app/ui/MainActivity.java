package com.altus.exam.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.altus.exam.app.MyFireabase;
import com.altus.exam.app.R;
import com.altus.exam.app.db.EmployeeDbManager;
import com.altus.exam.app.model.Employee;
import com.altus.exam.app.presenter.MainContract;
import com.altus.exam.app.presenter.MainPresenter;
import com.altus.exam.app.ui.adapter.EmployeeAdapter;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class MainActivity extends BaseAppCompatActivity implements MainContract.View, MainPresenter.OnMainListener{

    private MainContract.Presenter mainPresenter;
    private EmployeeAdapter adapter;

    @BindView(R.id.employees)
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Altus", "onCreate()");
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        mainPresenter = new MainPresenter(this, this);
        mainPresenter.setMainListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(AddUpdateEmployeeActivity.class);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainPresenter.loadAllEmployee();
    }

    @OnItemClick(R.id.employees)
    public void onItemClick(int position) {
        mainPresenter.getSelectedEmployee(adapter.getItem(position));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Altus", "onStart()");
        FirebaseUser firebaseUser = MyFireabase.getFirebaseAuth().getCurrentUser();
        if(firebaseUser == null){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_sign_out) {
            MyFireabase.getFirebaseAuth().signOut();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void handleAllEmployee(List<Employee> employees) {
        adapter = new EmployeeAdapter(this, employees);
        listView.setAdapter(adapter);
    }
}
