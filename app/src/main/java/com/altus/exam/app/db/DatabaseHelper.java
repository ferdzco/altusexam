package com.altus.exam.app.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.altus.exam.app.model.Employee;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE = "test.db";
    private static final int DB_VERSION = 1;

    private Dao<Employee, Integer> mEmployeeDao = null;



    public DatabaseHelper(Context context){
        super(context, DATABASE, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, Employee.class);
        }catch(SQLException e){
            throw new RuntimeException(e);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Employee.class, true);
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    public Dao<Employee, Integer> getEmployeeDao() throws SQLException{
        if(mEmployeeDao == null){
            mEmployeeDao = getDao(Employee.class);
        }
        return  mEmployeeDao;
    }
}