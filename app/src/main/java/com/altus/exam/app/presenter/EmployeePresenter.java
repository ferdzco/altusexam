package com.altus.exam.app.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.altus.exam.app.MyFireabase;
import com.altus.exam.app.db.EmployeeDbManager;
import com.altus.exam.app.model.Employee;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class EmployeePresenter implements EmployeeContract.Presenter{

    private EmployeeContract.View employeeView;
    private Context context;
    private OnEmployeeListener listener;


    public EmployeePresenter(Context context, EmployeeContract.View view){
        this.employeeView = view;
        this.context = context;
    }

    @Override
    public void setEmployeeListener(OnEmployeeListener listener) {
        this.listener = listener;
    }

    @Override
    public void loadExistingEmployee(int id) {
        Employee employee = EmployeeDbManager.getInstance().getEmployeeData(String.valueOf(id));
        listener.handleExistingEmployee(employee);
    }

    @Override
    public int saveEmployee(String firtsname, String lastname, String email) {
        Employee newEmployee = new Employee(firtsname, lastname, email);
        return EmployeeDbManager.getInstance().saveEmployee(newEmployee);
    }


    @Override
    public int updateEmployee(int id, String firtsname, String lastname, String email) {
        Employee employee = EmployeeDbManager.getInstance().getEmployeeData(String.valueOf(id));
        employee.setFirstname(firtsname);
        employee.setLastname(lastname);
        employee.setEmail(email);
        return EmployeeDbManager.getInstance().updateEmployee(employee.getId(), employee);
    }

    @Override
    public int deleteEmployee(int id) {
        Employee employee = EmployeeDbManager.getInstance().getEmployeeData(String.valueOf(id));
        return EmployeeDbManager.getInstance().deleteEmployee(employee);
    }


    public interface OnEmployeeListener {
        void handleExistingEmployee(Employee emp);
    }
}
