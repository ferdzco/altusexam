package com.altus.exam.app.ui;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;


public class BaseAppCompatActivity extends AppCompatActivity {

    public void showSnackbarInfo(View view, String message){
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    protected void toastMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void startActivity(Class clz){
        startActivity(new Intent(this, clz));
    }
}
