package com.altus.exam.app.presenter;


import android.content.Context;
import android.support.annotation.NonNull;

import com.altus.exam.app.MyFireabase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

public class RegisterPresenter implements RegisterContract.Presenter,  OnCompleteListener<AuthResult> {


    private RegisterContract.View registerView;
    private Context context;
    private RegisterPresenter.OnRegisterListener listener;


    public RegisterPresenter(Context context, RegisterContract.View view){
        this.registerView = view;
        this.context = context;
    }


    @Override
    public void setRegisterListener(OnRegisterListener listener) {
        this.listener = listener;
    }

    @Override
    public void register(String email, String password) {
        registerView.showProgressDialog();
        MyFireabase.getFirebaseAuth().createUserWithEmailAndPassword(email, password).
                addOnCompleteListener(this);

    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        if (task.isSuccessful()) {
            listener.handleRegisternSuccess();
        } else {
            listener.habdleRegisterFailed("Register failed.");
        }
        registerView.dismissProgressDialog();
    }

    public interface OnRegisterListener {
        void handleRegisternSuccess();
        void habdleRegisterFailed(String message);
    }

}
