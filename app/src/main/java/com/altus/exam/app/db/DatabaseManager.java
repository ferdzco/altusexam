package com.altus.exam.app.db;

import android.content.Context;

import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;


public class DatabaseManager {

    private DatabaseHelper helper;

    protected DatabaseManager(Context ctx) {
        helper = new DatabaseHelper(ctx);
    }

    protected DatabaseHelper getHelper() {
        return helper;
    }

    protected String rawQuery(final String tableName, final String[] selections,
                              final String whereClause, final String[] whereArgs){
        return rawQuery(tableName, selections, whereClause, whereArgs, null, null, false);
    }

    protected String rawQuery(final String tableName, final String[] selections,
                              final String whereClause, final String[] whereArgs, String groupBy,
                              String orderBy, boolean asc){

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT");

        if(selections == null){
            sb.append(" * ");
        }else{
            int selectionSize = 0;
            for(String column : selections){
                sb.append(" "+column);
                selectionSize++;
                if(selectionSize != selections.length){
                    sb.append(",");
                }
            }
        }

        sb.append(" FROM");
        sb.append(" "+tableName);

        //where clause
        whereClause(sb, whereClause, whereArgs);
        groupBy(sb, groupBy);
        orderBy(sb, orderBy, asc);

        sb.append(";");
        return sb.toString();
    }

    private void orderBy(StringBuilder sb, String orderBy, boolean asc) {
        if(orderBy != null){
            sb.append(" ORDER BY");
            sb.append(" "+ orderBy);
            sb.append(asc ? " ASC" : " DEC");
        }
    }

    private void groupBy(StringBuilder sb, String groupBy) {
        if(groupBy != null){
            sb.append(" GROUP BY");
            sb.append(" "+ groupBy);
        }
    }

    private void whereClause(StringBuilder sb, String where, String[] arg) {
        if(where != null){
            sb.append(" WHERE");
            String[] n = where.split("AND");
            int i = 0;
            for (String string : n) {
                sb.append(" "+string.replace("?", "'" +arg[i]+"'"));
                i++;
                if(i != arg.length){
                    sb.append("AND");
                }

            }
        }
    }

}