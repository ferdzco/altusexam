package com.altus.exam.app.presenter;


public interface RegisterContract {

    interface View{
        void showProgressDialog();
        void dismissProgressDialog();
        void showToastMessage(String text);
    }

    interface Presenter{
        void setRegisterListener(RegisterPresenter.OnRegisterListener listener);
        void register(String email, String password);
    }

}
