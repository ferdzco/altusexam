package com.altus.exam.app.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.altus.exam.app.R;
import com.altus.exam.app.model.Employee;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmployeeAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<Employee> Employees;


    public EmployeeAdapter(Context context, List<Employee> Employees){
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.Employees = Employees;
    }


    @Override
    public int getCount() {
        return Employees.size();
    }

    @Override
    public Employee getItem(int position) {
        return Employees.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent ) {
        ViewHolder mViewHolder = null;

        if(mViewHolder == null){
            view = inflater.inflate(R.layout.employee_item_layout, parent, false);

            mViewHolder = new ViewHolder(view);
            view.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder) view.getTag();
        }

        Employee emp = Employees.get(i);

        mViewHolder.tvFullname.setText(emp.getFirstname() + " "+ emp.getLastname());
        mViewHolder.tvEmail.setText(emp.getEmail());

        return view;
    }

    static class ViewHolder {
        @BindView(R.id.fullname) TextView tvFullname;
        @BindView(R.id.email) TextView tvEmail;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
