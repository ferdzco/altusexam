package com.altus.exam.app.presenter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.altus.exam.app.MyFireabase;
import com.altus.exam.app.db.EmployeeDbManager;
import com.altus.exam.app.model.Employee;
import com.altus.exam.app.ui.AddUpdateEmployeeActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View loginView;
    private Context context;
    private OnMainListener listener;


    public MainPresenter(Context context, MainContract.View view){
        this.loginView = view;
        this.context = context;
    }

    public void setMainListener(OnMainListener listener) {
        this.listener = listener;
    }

    @Override
    public void loadAllEmployee() {
        List<Employee> employees = EmployeeDbManager.getInstance().getAllEmployee();
        listener.handleAllEmployee(employees);

    }

    @Override
    public void getSelectedEmployee(Employee emp) {
        Intent intent = new Intent(context, AddUpdateEmployeeActivity.class);
        intent.putExtra("action", "Update");
        intent.putExtra("employeeId", emp.getId());
        context.startActivity(intent);
    }


    public interface OnMainListener {
        void handleAllEmployee(List<Employee> employees);
    }
}
