package com.altus.exam.app.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;


@DatabaseTable(tableName = Employee.TABLE_NAME_EMPLOYEE)
public class Employee implements Serializable {

    public static final String TABLE_NAME_EMPLOYEE = "employee";

    public static final String FIELD_EMPLOYEE_ID = "id";
    public static final String FIELD_FIRSTNAME = "firstname";
    public static final String FIELD_LASTNAME = "lastname";
    public static final String FIELD_EMAIL = "email";

    @DatabaseField(columnName = FIELD_EMPLOYEE_ID, generatedId = true)
    private int id;

    @DatabaseField(columnName = FIELD_FIRSTNAME)
    private String firstname;

    @DatabaseField(columnName = FIELD_LASTNAME)
    private String lastname;

    @DatabaseField(columnName = FIELD_EMAIL)
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firtname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public Employee(String firstname, String lastname, String email) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    public Employee(){}
}
