package com.altus.exam.app;

import android.app.Application;

import com.altus.exam.app.db.EmployeeDbManager;


public class ApplciationManager extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MyFireabase.init(this);

        //init employee manager
        EmployeeDbManager.init(this);
    }

}
