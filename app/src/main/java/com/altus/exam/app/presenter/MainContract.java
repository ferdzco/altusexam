package com.altus.exam.app.presenter;

import com.altus.exam.app.model.Employee;

public interface MainContract {
    interface View {
    }

    interface Presenter {
        void setMainListener(MainPresenter.OnMainListener listener);
        void loadAllEmployee();
        void getSelectedEmployee(Employee emp);
    }
}

