package com.altus.exam.app;

import android.content.Context;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;

public class MyFireabase {

    private static FirebaseAuth firebaseAuth;

    private MyFireabase(){}

    public static void init(Context context) {
        FirebaseApp.initializeApp(context);
        firebaseAuth = FirebaseAuth.getInstance();
    }

    public static FirebaseAuth getFirebaseAuth(){
        if(firebaseAuth != null){
            return firebaseAuth;
        }else {
            throw new IllegalStateException("FirebaseAuth not initialized");
        }
    }
}
