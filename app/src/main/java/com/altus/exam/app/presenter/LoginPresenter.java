package com.altus.exam.app.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.altus.exam.app.MyFireabase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginPresenter implements LoginContract.Presenter, OnCompleteListener<AuthResult> {

    private LoginContract.View loginView;
    private Context context;
    private OnLoginListener listener;


    public LoginPresenter(Context context, LoginContract.View view){
        this.loginView = view;
        this.context = context;
    }

    @Override
    public void setLoginListener(OnLoginListener listener) {
        this.listener = listener;
    }

    @Override
    public void signIn(String email, String password) {
        loginView.showProgressDialog();
        if(TextUtils.isEmpty(email) && TextUtils.isEmpty(password)){
            loginView.showToastMessage("Invalid usename / password");
            loginView.dismissProgressDialog();
        }

        final FirebaseAuth auth = MyFireabase.getFirebaseAuth();
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this);
    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        loginView.dismissProgressDialog();
        if (task.isSuccessful()) {
            Log.d("Altus", "signInWithEmail:success");
            //loginView.showToastMessage("Success ");
            listener.handleLoginSuccess();
        } else {
            Log.w("Altus", "signInWithEmail:failure", task.getException());
            //loginView.showToastMessage("Authentication failed.");
            listener.habdleLoginFailed("Authentication failed.");
        }
    }

    public interface OnLoginListener {
        void handleLoginSuccess();
        void habdleLoginFailed(String message);
    }
}
